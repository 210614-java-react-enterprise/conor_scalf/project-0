create table users
(
	username character varying(20) primary key not null,
	pword character varying(20) not null,
	
	fname character varying(20) not null,
	middle_initial character(1),
	lname character varying(20),
	
	creation_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP, 
	
	constraint unsername_minimum_length check (length(username)>=5),
	constraint pword_minimum_length check (length(pword)>=8)
);

create table accounts
(
	account_type character varying(20),
	account_holder character varying(20),
	funds bigint,

	constraint fk_account_holder foreign key(account_holder) references users(username),
	constraint no_negative_funds check(funds>=0),

	primary key(account_type,account_holder)
);

create table transactions
(
	amount integer,
	transaction_date timestamp with time zone default current_timestamp,
	
	transaction_owner character varying(20),
	constraint fk_transaction_owner foreign key(transaction_owner) references users(username),
	
	primary key(transaction_owner,transaction_date)
);