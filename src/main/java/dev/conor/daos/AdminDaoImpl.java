package dev.conor.daos;

import dev.conor.models.UserModel;

import java.sql.*;

public class AdminDaoImpl implements AdminDao
{
    public AdminDaoImpl()
    {

    }

    private Connection getConnection() throws SQLException
    {
        //Setting up the database connection  requires this form of URL:
        //jdbc:postgresql://host:port/database
        String url = "jdbc:postgresql://revature-training-db.c3kefanadszg.us-east-2.rds.amazonaws.com:5432/postgres";

        Connection connection = DriverManager.getConnection(url,System.getenv("DBUSER"),System.getenv("DBPASS"));

        return connection;
    }

    @Override
    public String userExists(String username)
    {
        return null;
    }

    @Override
    public String createUser(UserModel newUser, String password)
    {


        //Attempt to add the user into the database
        //String sql = "insert into users (username,pword,fname,lname) values (\'" + newUser.getUsername() + "\',\'"+password+"\',\'" + newUser.getFname() + "\',\'Johnson\');";
        String sql = "insert into users (username,pword,fname) values (?,?,?)";
        try(Connection connection = getConnection();
            PreparedStatement insertStatement = connection.prepareStatement(sql);)
        {
            insertStatement.setString(1,newUser.getUsername());
            insertStatement.setString(2,password);
            insertStatement.setString(3,newUser.getFname());

            int success = insertStatement.executeUpdate();
            if (success>0)
                System.out.println("New user created! Welcome to OMNIBANK!\n" +
                                   "Please login to manage your account with us;\n" +
                                   "we hope you enjoy our services!");
        } catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public String removeUser(String username)
    {
        return null;
    }

    @Override
    public String addUserAccount(String username, String accountType)
    {
        return null;
    }

    @Override
    public String removeUserAccount(String username, String accountType)
    {
        return null;
    }

    @Override
    public String removeAllSubAccounts(String username)
    {
        return null;
    }
}
