package dev.conor.daos;

import dev.conor.models.UserModel;

import java.sql.Connection;

public interface AdminDao
{
    public String userExists(String username);

    public String createUser(UserModel newUser, String password);
    public String removeUser(String username);

    public String addUserAccount(String username, String accountType);
    public String removeUserAccount(String username, String accountType);
    public String removeAllSubAccounts(String username);
}
