package dev.conor.daos;

import dev.conor.models.UserModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

//Handles database connections, and sending and retrieving data for the client
public class UserDaoImpl implements UserDao
{
    public UserDaoImpl()
    {

    }

    private Connection getConnection() throws SQLException
    {
        //Setting up the database connection  requires this form of URL:
        //jdbc:postgresql://host:port/database
        String url = "jdbc:postgresql://revature-training-db.c3kefanadszg.us-east-2.rds.amazonaws.com:5432/postgres";

        Connection connection = DriverManager.getConnection(url,System.getenv("DBUSER"),System.getenv("DBPASS"));

        return connection;
    }

    public UserModel checkLogin(String username,String password)
    {
        //Connect to database
        try(Connection connection = getConnection();)
        {
            //Check if username/password combo matches
            String sql = "select username, fname from users where username = (?) and pword = (?)";
            PreparedStatement recordExists = connection.prepareStatement(sql);
            recordExists.setString(1,username);
            recordExists.setString(2,password);
            ResultSet rs = recordExists.executeQuery();
            if (rs.next())
            {
                //Welcome message
                System.out.println("--------------------------------------\n" +
                                   "Welcome to your account, " + rs.getString("fname") + ".\n" +
                                   "--------------------------------------");
                //Retrieve all of the users accounts, if any
                String sql2 = "select * from accounts where account_holder = (?)";
                PreparedStatement accountQuery = connection.prepareStatement(sql2);
                accountQuery.setString(1,username);
                ResultSet userAccounts = accountQuery.executeQuery();

                HashMap<String,Integer> currentAccounts = new HashMap<String,Integer>();
                while (userAccounts.next())
                {
                    currentAccounts.put(userAccounts.getString("account_type"),userAccounts.getInt("funds"));
                }

                return new UserModel(rs.getString("username"),rs.getString("fname"),currentAccounts);
            }
            else
            {
                return null;
            }
        } catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Connection failed.");
            //Otherwise return null
            return null;
        }
    }
    public UserModel checkLogin(String username)
    {
        //Connect to database
        try(Connection connection = getConnection();)
        {
            //Check if username/password combo matches
            String sql = "select username, fname from users where username = (?)";
            PreparedStatement recordExists = connection.prepareStatement(sql);
            recordExists.setString(1,username);
            ResultSet rs = recordExists.executeQuery();
            if (rs.next())
            {
                //Welcome message
                System.out.println("--------------------------------------\n" +
                        "Welcome to your account, " + rs.getString("fname") + ".\n" +
                        "--------------------------------------");
                //Retrieve all of the users accounts, if any
                String sql2 = "select * from accounts where account_holder = (?)";
                PreparedStatement accountQuery = connection.prepareStatement(sql2);
                accountQuery.setString(1,username);
                ResultSet userAccounts = accountQuery.executeQuery();

                HashMap<String,Integer> currentAccounts = new HashMap<String,Integer>();
                while (userAccounts.next())
                {
                    currentAccounts.put(userAccounts.getString("account_type"),userAccounts.getInt("funds"));
                }

                return new UserModel(rs.getString("username"),rs.getString("fname"),currentAccounts);
            }
            else
            {
                return null;
            }
        } catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Connection failed.");
            //Otherwise return null
            return null;
        }
    }

    public void addAccount(String accountName,String username)
    {
        String sql = "insert into accounts (account_type,account_holder,funds) values (?,?,0)";
        try (Connection connection = getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(sql);)
        {
            insertStatement.setString(1,accountName);
            insertStatement.setString(2,username);

            int success = insertStatement.executeUpdate();
            if (success>0)
                System.out.println("New account created successfully! Please add money...");
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Failed to add account to database.");
        }
    }

    @Override
    public ArrayList<String> getAllSubAccounts()
    {
        return null;
    }

    @Override
    public String getAccountFname()
    {
        return null;
    }

    @Override
    public String getAccountLname()
    {
        return null;
    }

    @Override
    public int getAccountBalance()
    {
        return 0;
    }

    @Override
    public int getAccountBalance(String accountType)
    {
        return 0;
    }

    @Override
    public String depositFunds(int amount,String targetUser, String targetAccount, int currentFunds)
    {
        if (amount==0)
        {
            System.out.println("Invalid deposit amount; deposit cannot be zero!");
            return null;
        }
        else if(amount<0)
        {
            System.out.println("Invalid deposit amount; deposit cannot be negative!");
            return null;
        }

        String sql = "update accounts set funds = (?) where account_holder = (?) AND account_type = (?)";
        try (Connection connection = getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(sql);)
        {
            insertStatement.setInt(1,currentFunds + amount);
            insertStatement.setString(2,targetUser);
            insertStatement.setString(3,targetAccount);

            int success = insertStatement.executeUpdate();
            if (success>0)
                System.out.println("Deposit successful!  We appreciate your business...");
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Failed to add account to database.");
        }

        return null;
    }

    @Override
    public String withdrawFunds(int amount,String targetUser, String targetAccount, int currentFunds)
    {
        if (amount==0)
        {
            System.out.println("Invalid deposit amount; withdrawal cannot be zero!");
            return null;
        }
        else if(amount<0)
        {
            System.out.println("Invalid withdrawal amount; withdrawal cannot be negative!");
            return null;
        }
        else if (currentFunds<amount)
        {
            System.out.println("Invalid withdrawal amount; insufficient funds in account!");
            return null;
        }
        else
            alterFunds(amount,targetUser,targetAccount,currentFunds,false);

        return "success";
    }

    private String alterFunds(int amount,String targetUser, String targetAccount, int currentFunds, boolean isDeposit)
    {

        String sql = "update accounts set funds = (?) where account_holder = (?) AND account_type = (?)";
        try (Connection connection = getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(sql);)
        {
            if (isDeposit)
                insertStatement.setInt(1,currentFunds + amount);
            else
                insertStatement.setInt(1,currentFunds - amount);

            insertStatement.setString(2,targetUser);
            insertStatement.setString(3,targetAccount);

            int success = insertStatement.executeUpdate();
            if (success>0)
                System.out.println("Withdrawal successful!  We appreciate your business...");
            return "success";
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
            System.out.println("Failed to add account to database.");
        }
        return null;
    }
}
