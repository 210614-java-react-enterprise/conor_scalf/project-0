package dev.conor.daos;

import java.util.ArrayList;

public interface UserDao
{
    ArrayList<String> getAllSubAccounts();

    String getAccountFname();
    String getAccountLname();

    int getAccountBalance();
    int getAccountBalance(String accountType);

    String depositFunds(int amount,String targetUser, String targetAccount, int currentFunds);
    String withdrawFunds(int amount, String targetUser, String targetAccount, int currentFunds);
}
