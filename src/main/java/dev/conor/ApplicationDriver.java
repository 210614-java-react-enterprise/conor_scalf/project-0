package dev.conor;

import dev.conor.controllers.Controller;

public class ApplicationDriver
{
    public static void main(String[] args)
    {
        System.out.println("  OMNIBANK==OMNIBANK==OMNIBANK==OMNIBANK==OMNIBANK  " +
                           " ================================================== " +
                           "=======================WELCOME======================\n" +
                           "============TO OUR ONLINE BANKING SYSTEM============");
        //Create the controller, which facilitates everything; it will immediately create a StartupView loop object,
        //which will ask the user if they want to login or exit.
        Controller controller = new Controller();

        //Start the user input process
        controller.startApplication();

        //====================================================================================
        //====================================================================================


        //Set up an "application loop" to handle user input until they exit
        //APPLICATION LOOP:
    }
}
