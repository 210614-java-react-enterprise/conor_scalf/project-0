package dev.conor.views;

public class LandingMenu extends InputLoopMenu
{
    public LandingMenu()
    {
        super();
        validInputs.add("Login");
        validInputs.add("Create Account");
    }
}
