package dev.conor.views;

import java.util.HashMap;

public class MenuFromArray extends InputLoopMenu
{
    public MenuFromArray(String[] items)
    {
        for (String item : items)
        {
            validInputs.add(item);
        }
    }
}
