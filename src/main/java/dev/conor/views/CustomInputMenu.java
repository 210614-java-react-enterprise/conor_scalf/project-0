package dev.conor.views;

public class CustomInputMenu extends InputLoopMenu
{
    public CustomInputMenu()
    {
        //This is just a menu variant that allows the user to enter a custom string,
        //that will be accepted, while also guarding against bad character inputs.
    }

    @Override
    protected String isValidInput(String input)
    {
        System.out.println("Checking for illegal input...");
        //If the user inputs an acceptable string, return it.  Otherwise, this loops until they do.
        for (char ch : input.toCharArray())
        {
            //If not alphabetic or numeric, reject
            if ((!Character.isAlphabetic(ch))&&(!Character.isDigit(ch)))
            {
                System.out.println("Illegal character in input.");
                return "invalid";
            }
        }
        //Check to see if the user wanted to perform an operation on the menu list
        if(!super.isValidInput(input).equals("invalid"))
        {
            //Tell the controller to do the operation on the menu list
            return super.isValidInput(input);
        }
        //No invalid characters and not a normal menu choice, so the chosen string is good to go.
        return input;
    }
}
