package dev.conor.views;

import dev.conor.interfaces.UserAccountInterface;
import dev.conor.models.UserModel;

public class MainMenu extends InputLoopMenu implements UserAccountInterface
{
    UserModel currentUser;
    public MainMenu(UserModel currentUser)
    {
        this.currentUser = currentUser;

        validInputs.add("Check Balance");
        validInputs.add("Deposit");
        validInputs.add("Withdraw");
        validInputs.add("Create banking account");
    }

    @Override
    public String getSummary()
    {
        //Implement this last; it should show all of the users accounts, as well as their balances
        //Later, if we have time, it can show additional details for each, like number of transactions
        //in the last 30 days, when the accounts were created, etc
        return null;
    }

    @Override
    public int checkBalance()
    {
        return 0;
    }

    @Override
    public int deposit(int amount)
    {
        return 0;
    }

    @Override
    public int withdraw(int amount)
    {
        return 0;
    }

    @Override
    public String getAccountDetails()
    {
        return null;
    }
}
