package dev.conor.views;

import java.util.ArrayList;
import java.util.Scanner;

public class InputLoopMenu
{
    protected ArrayList<String> validInputs;
    protected Scanner inputScanner;
    protected String lastInput;

    public InputLoopMenu()
    {
        //Initialize with the valid inputs that are common to any area in our console application
        //Exit is the only default
        validInputs = new ArrayList<>();
        validInputs.add("Exit");

        inputScanner = new Scanner(System.in);
    }

    public String getInput()
    {
        return getInputCustomPrompt("","");
    }
    public String getInputCustomPrompt(String textBeforeMenu, String textAfterMenu)
    {
        printMenuCustomText(textBeforeMenu,textAfterMenu);

        String validInput = "invalid";
        while(validInput=="invalid")
        {
            //Loop until the user chooses a valid input option
            lastInput = inputScanner.nextLine();

            validInput = isValidInput(lastInput);
            if (validInput.equals("invalid"))
            {
                System.out.println("Invalid input.  Please try again.");
            }
        }
        return validInput;
    }

    protected String isValidInput(String input)
    {
        System.out.println("Input recieved: " + input);
        input = input.toLowerCase();
        //Loops through the options in the valid input list to try to find a match
        for (int x=0; x<validInputs.size(); x++)
        {
            //Checks if the user input was a valid string, OR the number index of one in validInputs
            if((input.equals(validInputs.get(x).toLowerCase()))||(validInputs.indexOf(input)==x))
            {
                System.out.println("Valid input: " + validInputs.get(x));
                return validInputs.get(x).toLowerCase();
            }
        }

        return "invalid";
    }

    public void printMenu()
    {
        printMenuCustomText("Please choose an option below:","");
    }
    public void printMenuCustomText(String textBeforeMenu, String textAfterMenu)
    {
        if (!textBeforeMenu.equals(""))
            System.out.println(textBeforeMenu);
        for (int x=0; x<validInputs.size(); x++)
        {
            System.out.println(x + ". " + validInputs.get(x));
        }
        if (!textAfterMenu.equals(""))
            System.out.println(textAfterMenu);

        System.out.println("====================================================");
        System.out.println("====================================================");
    }
}
