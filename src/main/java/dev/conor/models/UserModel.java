package dev.conor.models;

import java.util.ArrayList;
import java.util.HashMap;

public class UserModel
{
    private String username;
    private String fname;
    private String lname;
    private HashMap<String,Integer> accounts;

    public UserModel(String username, String fname, HashMap<String,Integer> accounts)
    {
        this.username = username;
        this.fname = fname;
        this.accounts = accounts;
    }
    public UserModel(String username, String fname)
    {
        this.username = username;
        this.fname = fname;
    }

    public String getUsername()
    {
        return username;
    }

    public String getFname()
    {
        return fname;
    }

    public String getLname()
    {
        return lname;
    }

    public HashMap<String,Integer> getAccounts()
    {
        return accounts;
    }
}
