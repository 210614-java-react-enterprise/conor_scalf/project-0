package dev.conor.interfaces;

public interface UserAccountInterface
{
    public String getSummary();

    public int checkBalance();

    public int deposit(int amount);
    public int withdraw(int amount);

    public String getAccountDetails();
}
