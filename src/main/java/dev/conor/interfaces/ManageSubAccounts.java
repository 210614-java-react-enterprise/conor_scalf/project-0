package dev.conor.interfaces;

public interface ManageSubAccounts
{
    public String openAccount(String user, String accountType);

    public String closeAccount(String user, String accountType);
}
