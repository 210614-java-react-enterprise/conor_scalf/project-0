package dev.conor.controllers;

import dev.conor.daos.AdminDaoImpl;
import dev.conor.daos.UserDaoImpl;
import dev.conor.models.UserModel;
import dev.conor.views.*;

import java.sql.SQLException;
import java.util.Iterator;

public class Controller
{
    private InputLoopMenu landingMenuLoop;
    private UserModel userModel;
    private String lastInput;
    private boolean applicationRunning;

    public Controller()
    {
        applicationRunning = true;

        //Start the program's process of getting input
        landingMenuLoop = new LandingMenu();
    }

    public void startApplication()
    {
        //This is just a placeholder; this just starts the overall program loop of the application
        mainMenuLoop();
    }

    private void mainMenuLoop()
    {
        while(applicationRunning)
        {
            lastInput =  landingMenuLoop.getInputCustomPrompt("   MAIN MENU\n" +
                    "Please choose an option below.","");
            mainMenuProcessInput();
        }
    }
    private void loggedInInputLoop(UserModel currentUser)
    {
        boolean loggedIn = true;

        MainMenu userMenu = new MainMenu(currentUser);
        String userInput;

        //Keep looping the menu until the user chooses to exit
        while (loggedIn)
        {
            //Make a note to the user if they currently have no accounts
            String statusMessage="";
            if (currentUser.getAccounts().size()==0)
                statusMessage = "\n***Please note: you do not currently have any\nbanking accounts set up with us.";

            userInput = userMenu.getInputCustomPrompt("How can we help you today?" + statusMessage,"");

            if (userInput.equals("exit"))
                loggedIn = false;
            else
            {
                switch (userInput)
                {
                    case "check balance":
                        String[] newMenuOptions = currentUser.getAccounts().keySet().toArray(new String[currentUser.getAccounts().size()]);

                        MenuFromArray selectAccount = new MenuFromArray(newMenuOptions);
                        if (currentUser.getAccounts().size()==0)
                        {
                            System.out.println("NO USER ACCOUNTS DETECTED!");
                        }
                        else if(currentUser.getAccounts().size()==1)
                        {
                            //Get the only key of the only account
                            Iterator<String> iterator = currentUser.getAccounts().keySet().iterator();
                            String accountName = iterator.next();
                            Integer balance = currentUser.getAccounts().get(accountName);

                            System.out.println("ACCOUNT NAME: " + accountName);
                            System.out.println("ACCOUNT BALANCE: " + generateBalanceOutput(balance));
                        }
                        else
                        {
                            Iterator<String> iterator = currentUser.getAccounts().keySet().iterator();
                            while (iterator.hasNext())
                            {
                                String accountName = iterator.next();
                                Integer balance = currentUser.getAccounts().get(accountName);

                                System.out.println("ACCOUNT NAME: " + accountName);
                                System.out.println("ACCOUNT BALANCE: " + generateBalanceOutput(balance));
                            }
                        }
                        break;
                    case "deposit":
                        UserDaoImpl depositController = new UserDaoImpl();

                        CustomInputMenu getDepositInformation = new CustomInputMenu();

                        boolean validDepositAccount = false;
                        String targetDAccount="";
                        while (validDepositAccount==false)
                        {
                            targetDAccount = getDepositInformation.getInputCustomPrompt("Please enter the name of the account for the deposit:","");
                            for (String acct : currentUser.getAccounts().keySet())
                            {
                                if (acct.equals(targetDAccount))
                                    validDepositAccount = true;
                            }
                            if (validDepositAccount ==false)
                                System.out.println("Invalid deposit account.  Please try again.");
                        }

                        boolean validDeposit = false;
                        int depositAmount=0;
                        while (validDeposit==false)
                        {
                            String depositString = getDepositInformation.getInputCustomPrompt("Please enter a deposit amount.","");
                                try
                                {
                                    depositAmount = Integer.parseInt(depositString+"00");
                                    validDeposit = true;
                                }
                                catch (NumberFormatException e)
                                {
                                    System.out.println("Invalid number format.  Please try again.");
                                }
                        }

                        depositController.depositFunds(depositAmount,currentUser.getUsername(),targetDAccount,currentUser.getAccounts().get(targetDAccount));

                        break;
                    case "withdraw":
                        UserDaoImpl withdrawalController = new UserDaoImpl();

                        CustomInputMenu getWithdrawalInformation = new CustomInputMenu();

                        boolean validWithdrawalAccount = false;
                        String targetWAccount="";
                        while (validWithdrawalAccount==false)
                        {
                            targetWAccount = getWithdrawalInformation.getInputCustomPrompt("Please enter the name of the account for the withdrawal:","");
                            for (String acct : currentUser.getAccounts().keySet())
                            {
                                if (acct.equals(targetWAccount))
                                    validWithdrawalAccount = true;
                            }
                            if (validWithdrawalAccount ==false)
                                System.out.println("Invalid withdrawal account.  Please try again.");
                        }

                        boolean validWithdrawal = false;
                        int withdrawalAmount=0;
                        while (validWithdrawal==false)
                        {
                            String withdrawalString = getWithdrawalInformation.getInputCustomPrompt("Please enter a withdrawal amount.","");
                            try
                            {
                                withdrawalAmount = Integer.parseInt(withdrawalString+"00");
                                validWithdrawal = true;
                            }
                            catch (NumberFormatException e)
                            {
                                System.out.println("Invalid number format.  Please try again.");
                            }
                        }

                        withdrawalController.withdrawFunds(withdrawalAmount,currentUser.getUsername(),targetWAccount,currentUser.getAccounts().get(targetWAccount));

                        break;

                    case"account summary":

                        break;

                    case "create banking account":
                        UserDaoImpl accountMaker = new UserDaoImpl();

                        CustomInputMenu getNameForAccount = new CustomInputMenu();
                        String newAccountName = getNameForAccount.getInputCustomPrompt("Please enter a name for your new banking account.\n" +
                                                                                                    "Only letters and numbers, please.","");

                        accountMaker.addAccount(newAccountName,currentUser.getUsername());
                        break;
                }
            }

            //Regardless of what command we executed, update the user account:
            currentUser = new UserDaoImpl().checkLogin(currentUser.getUsername());
        }
        System.out.println("Returning to main menu...");
    }

    private String generateBalanceOutput(Integer balance)
    {
        String balanceOutput = balance.toString();

        if (balanceOutput.length()==1)
            balanceOutput = "00" + balanceOutput;
        else if (balanceOutput.length()==2)
            balanceOutput = "0" + balanceOutput;

        balanceOutput = balanceOutput.substring(0,balanceOutput.length()-2)+"."+balanceOutput.substring(balanceOutput.length()-2,balanceOutput.length());
        balanceOutput = "$" + balanceOutput;
        return balanceOutput;
    }

    private void quit()
    {
        applicationRunning = false;
    }

    private void mainMenuProcessInput()
    {
        switch (lastInput)
        {
            case "exit":
                System.out.println("Quitting...");
                quit();
                break;
            case "login":
                login();
                break;
            case "create account":
                createAccount();
                break;
            default:
                System.out.println("Something unusual has gone wrong with your input.  Exiting program...");
        }
    }

    private void createAccount()
    {
        //Create a brief connection to the server,
        AdminDaoImpl newUserUtility = new AdminDaoImpl();
        System.out.println("Successfully connected to database.");

        //Grab two inputs from user and validate them,
        System.out.println("Please enter your desired username, or exit.");
        System.out.println("Letters and numbers only.");
        CustomInputMenu newAccountPrompts = new CustomInputMenu();

        //The elements currently required by the database:
        String newUsername;
        String newPassword;
        String newFname;

        newUsername = newAccountPrompts.getInputCustomPrompt("Please enter a username with no special characters or\n" +
                "spaces. Alternatively, you can choose an option from\n" +
                "below:","");
        if (newUsername.equals("exit"))
            quit();
        else
        {
            newPassword = newAccountPrompts.getInputCustomPrompt("Please enter a password for your account.  Do\n" +
                    "not enter special characters or spaces.","");
            if (newPassword.equals("exit"))
                quit();
            else
            {
                //Prompt for first name
                newFname = newAccountPrompts.getInputCustomPrompt("Please enter your first name.  Do not enter\n" +
                        "special characters or spaces.","");
                if (newFname.equals("exit"))
                    quit();
                else
                {
                    //Create a user model out of the information, and send the model to the DAO for processing:
                    UserModel newUser = new UserModel(newUsername,newFname);
                    newUserUtility.createUser(newUser, newPassword);
                }
            }
        }
    }

    private void login()
    {
        UserDaoImpl loginAttempt = new UserDaoImpl();

        CustomInputMenu getCredentials = new CustomInputMenu();

        String username;
        String password;

        username = getCredentials.getInputCustomPrompt("Please enter your username, or\n" +
                                                                    "choose an option from below.","");
        if (!username.equals("exit"))
        {
            password = getCredentials.getInputCustomPrompt("Please enter your password, or\n" +
                                                                        "choose an option from below.","");
            if (!password.equals("exit"))
            {
                UserModel currentUser = loginAttempt.checkLogin(username,password);
                if (currentUser!=null)
                {
                    //Start the menu loop for logged in users
                    loggedInInputLoop(currentUser);
                }
                else
                {
                    System.out.println("USERNAME/PASSWORD MATCH NOT FOUND.");
                }
            }
        }
    }
}
